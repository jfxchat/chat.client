package org.hanyuu;
import javafx.fxml.FXML;

import javafx.scene.control.*;
import org.hanyuu.net.ChatMessage;
import org.hanyuu.net.Network;

import java.util.Random;

public class Controller
{
    @FXML private TextField message;
    @FXML public TextArea messages;
    private Network network;
    final private int clientId = new Random().nextInt(999);
    public Controller(){
        this.network = new Network(this);
        this.network.start();
    }
    public void addMessage(ChatMessage message)
    {
        this.messages.setText(String.format("%s\n[%s]%s",this.messages.getText(),message.clientId,message.message));
    }

    public void click()
    {
        String msg = this.message.getText();
        try {
            ChatMessage chatMessage = new ChatMessage();
            chatMessage.clientId = this.clientId;
            chatMessage.message = msg;
            this.network.sendMessage(chatMessage);
            this.message.setText("");
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
