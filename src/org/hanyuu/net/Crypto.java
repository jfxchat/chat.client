package org.hanyuu.net;

import java.security.SecureRandom;

/**
 * Created by Hanyuusha on 13.12.2016.
 */
public class Crypto {
    private long a;
    private long g;
    private long p;
    private long A;
    private long B;
    private long K; //Key
    public Crypto(){
        SecureRandom sr = new SecureRandom();
        this.a = sr.nextLong();
        this.g = sr.nextLong();
        this.p = sr.nextLong();
        A = Long.getLong(""+Math.pow(g,a))%p;
    }

    public long getA() {
        return a;
    }

    public long getG() {
        return g;
    }

    public long getP() {
        return p;
    }

}
