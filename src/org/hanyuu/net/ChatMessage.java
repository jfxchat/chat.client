package org.hanyuu.net;

/**
 * Created by Hanyuusha on 28.12.2016.
 */
import java.io.Serializable;
import java.util.Random;

public class ChatMessage implements Serializable
{
    protected static final long serialVersionUID = 1112122200L;

    public String message = "";
    public int clientId = -1;
    public int code = -1;

    @Override
    public String toString(){
        return String.format("[id:%s|messsage:%s|code:%s]",clientId,message,code);
    }
}