package org.hanyuu.net;
import org.hanyuu.Controller;
import java.net.*;
import java.io.*;
import java.security.SecureRandom;
import java.util.Random;

/**
 * Created by Hanyuusha on 20.11.2016.
 */

public class Network extends Thread
{


    private ObjectInputStream sInput;
    private ObjectOutputStream sOutput;
    private Socket socket;
    private Controller controller;
    private int status = 0;


    public Network(Controller controller){
        try {
            this.socket = new Socket("localhost",55);
            this.controller = controller;
            this.sOutput = new ObjectOutputStream(new DataOutputStream(this.socket.getOutputStream()));
            this.sInput = new ObjectInputStream(new DataInputStream(this.socket.getInputStream()));
        }catch(Exception e){
              e.printStackTrace();
        }
    }
    @Override
    public void run(){
        try {
            while (true){
                /*switch (this.status){
                    case 0:
                        ChatMessage message= new ChatMessage();
                        message.code = 0;
                        message.message = ""+ new SecureRandom().nextLong();
                        this.sendMessage(message);


                }*/
                ChatMessage message = (ChatMessage)this.sInput.readObject();
                this.controller.addMessage(message);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    public void sendMessage(ChatMessage message){
        try {
            this.sOutput.writeObject(message);
        }catch (Exception e){
            e.printStackTrace();
        }

    }

}
